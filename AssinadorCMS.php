<?php

// URL PARA A INICIALIZAÇÃO DA ASSINATURA CMS
const URLInicializacao = 'https://fw2.bry.com.br/api/cms-signature-service/v1/signatures/initialize';
// URL PARA A FINALIZAÇÃO DA ASSINATURA CMS
const URLFinalizacao = 'https://fw2.bry.com.br/api/cms-signature-service/v1/signatures/finalize';
// CAMINHO ONDE ESTÁ LOCALIZADO O ARQUIVO CMS A SER ASSINADO
const caminhoDoArquivoParaAssinar = '/caminhos/para/o/arquivo.pdf';
// TOKEN AUTHORIZATION GERADO NO BRY CLOUD
const token = "Bearer tokenAuthorization";
// CAMINHO ONDE ESTÁ LOCALIZADO O CERTIFICADO PKCS12
const caminhoDoCertificado = '/caminho/para/o/certificado.p12';
// SENHA DO CERTIFICADO
const senhaDoCertificado = 'senhaDoCertificado';

function inicializarAssinatura()
{

  list($certificadoTratado)= getCertificadoEChavePrivada();

  // CRIAÇÃO DA REQUISIÇÃO QUE SERÁ ENVIADA PARA A INICIALIZAÇÃO
  $curlInicializacao = curl_init();

  curl_setopt_array($curlInicializacao, array(
    CURLOPT_URL => URLInicializacao,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "POST",
    CURLOPT_POSTFIELDS => array(
      'originalDocuments[0][content]' => new CURLFILE(caminhoDoArquivoParaAssinar),
      'originalDocuments[0][nonce]' => '1',
      'nonce' => '1',
      'attached' => 'true',
      'operationType' => 'SIGNATURE',
      'profile' => 'ADRT',
      'hashAlgorithm' => 'SHA256',
      'certificate' => $certificadoTratado,
    ),
    CURLOPT_HTTPHEADER => array(
      "Authorization:" . token
    ),
  ));

  // ENVIA A REQUISIÇÃO
  $respostaInicializacao = curl_exec($curlInicializacao);
  curl_close($curlInicializacao);

  echo "RESPOSTA DA INICIALIZAÇÃO:\n\n";
  echo $respostaInicializacao;
  echo "\n\n__________________________________________________\n\n";
  return $respostaInicializacao;
}

function cifraInicialização($respostaInicializacao)
{

  //  PEGA A CHAVE PRIVADA DO CERTIFICADO
  list(,$privkey)= getCertificadoEChavePrivada();

  // EXTRAI O CONTEÚDO RETORNADO NA INICIALIZAÇÃO
  $content = getContentRespostaInicializacao($respostaInicializacao);

  // DECODIFICA O BASE64 QUE ESTÁ NA VARIÁVEL "messageDigest"
  $contentDecodificado = base64_decode($content);

  // CIFRA/ASSINA O CONTEÚDO QUE RETORNA DA INICIALIZAÇÃO COM A CHAVE PRIVADA DO CERTIFICADO
  $success = openssl_sign($contentDecodificado, $crypted, $privkey, 'sha256WithRSAEncryption');

  // CODIFICA EM BASE64 O CONTEÚDO RETORNADO DA CIFRAGEM DOS DADOS
  $cifrado = base64_encode($crypted);
  echo "CONTEÚDO CIFRADO:\n\n";
  echo $cifrado;
  echo "\n\n_________________________________________________\n\n";
  return $cifrado;
}

function finalizarAssinatura($cifrado, $respostaInicializacao)
{
  list($certificadoTratado) = getCertificadoEChavePrivada();
  $content = getContentRespostaInicializacao($respostaInicializacao);

  // CRIA A REQUISIÇÃO DA FINALIZAÇÃO DA ASSINATURA
  $curlFinalizacao = curl_init();

  curl_setopt_array($curlFinalizacao, array(
    CURLOPT_URL => URLFinalizacao,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "POST",
    CURLOPT_POSTFIELDS => array(
      'finalizations[0][document]' => new CURLFILE(caminhoDoArquivoParaAssinar),
      'finalizations[0][nonce]' => '1',
      'finalizations[0][signatureValue]' => $cifrado,
      'finalizations[0][signedAttributes]' => $content,
      'attached' => 'true',
      'nonce' => '1',
      'operationType' => 'SIGNATURE',
      'profile' => 'ADRT',
      'hashAlgorithm' => 'SHA256',
      'certificate' => $certificadoTratado,
    ),
    CURLOPT_HTTPHEADER => array(
      "Authorization:" . token
    ),
  ));

  // FAZ A REQUISIÇÃO DE FINALIZAÇÃO
  $respostaFinalizacao = curl_exec($curlFinalizacao);
  curl_close($curlFinalizacao);

  // IMPRIME A RESPOSTA DA FINALIZAÇÃO, QUE CONTÉM O LINK PARA DOWNLOAD DO ARQUIVO ASSINADO
  echo "RESPOSTA DA FINALIZAÇÃO:\n\n";
  echo $respostaFinalizacao;
  echo "\n\n__________________________________________________\n\n";

  echo "DOCUMENTO ASSINADO: \n\n";
  $obj = json_decode($respostaFinalizacao);
  echo $obj->signatures[0]->content;
  echo "\n\n__________________________________________________\n\n";
}

function getCertificadoEChavePrivada()
{
  // EXTRAI A CHAVE PRIVADA E O CONTEÚDO DO CERTIFICADO.
  $cert_store = file_get_contents(caminhoDoCertificado);
  var_dump(openssl_pkcs12_read($cert_store, $certs, senhaDoCertificado));
  $privkey = $certs['pkey'];
  $certificado = $certs['cert'];

  // RETIRA DA STRING DO CERTIFICADO PARTES NÃO DESEJADAS
  $certificadoTratado = str_replace('-----BEGIN CERTIFICATE-----', '', $certificado);
  $certificadoTratado = str_replace('-----END CERTIFICATE-----', '', $certificadoTratado);
  $certificadoTratado = str_replace("\n", '', $certificadoTratado);
  return array($certificadoTratado, $privkey);
}

function getContentRespostaInicializacao($respostaInicializacao)
{
    // MAPEIA O JSON QUE VOLTA COMO RESPOSTA DA REQUISIÇÃO
    $obj = json_decode($respostaInicializacao);
    // EXTRAI AS INFORMAÇÕES "messageDigest" e "nonce" DO RETORNO DA INICIALIZAÇÃO
    $content = $obj->signedAttributes[0]->content;

    return $content;
}

$respostaInicializacao =  inicializarAssinatura();
$cifrado = cifraInicialização($respostaInicializacao);
finalizarAssinatura($cifrado, $respostaInicializacao);